# SPDX-FileCopyrightText: 2022-2024 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

"""App settings
------------

This module defines the settings options for the
``djac-webpep`` app.
"""


from __future__ import annotations

from dasf_broker.app_settings import DASF_WEBSOCKET_URL_ROUTE
from django.conf import settings  # noqa: F401

#: Base route for the websocket routing of the
#: ``djac-webpep`` app
DJAC_WEBPEP_WEBSOCKET_URL_ROUTE = getattr(
    settings, "DJAC_WEBPEP_WEBSOCKET_URL_ROUTE", DASF_WEBSOCKET_URL_ROUTE
)


DJAC_WEBPEP_WEBPEP_TOPIC = getattr(
    settings, "DJAC_WEBPEP_WEBPEP_TOPIC", "webpep"
)


DJAC_WEBPEP_TOOLS_TOPIC = getattr(
    settings, "DJAC_WEBPEP_TOOLS_TOPIC", "clm-tools"
)
