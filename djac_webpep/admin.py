# SPDX-FileCopyrightText: 2022-2024 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

"""Admin interfaces
----------------

This module defines the djac-webpep
Admin interfaces.
"""


from django.contrib import admin  # noqa: F401

from djac_webpep import models  # noqa: F401
