# SPDX-FileCopyrightText: 2022-2024 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

"""Views
-----

Views of the djac-webpep app to be imported via the url
config (see :mod:`djac_webpep.urls`).
"""

from __future__ import annotations

import base64
import json
import random
import string
from typing import Any, Dict
from uuid import uuid4

from academic_community.utils import PermissionRequiredMixin
from dasf_broker.models import BrokerTopic, ResponseTopic
from django.shortcuts import get_object_or_404
from django.urls import reverse
from django.utils.functional import cached_property
from django.views import generic  # noqa: F401

from djac_webpep import app_settings  # noqa: F401
from djac_webpep import forms  # noqa: F401


def get_random_letters(length: int) -> str:
    return "".join(random.choice(string.ascii_letters) for i in range(length))


class WebPepView(PermissionRequiredMixin, generic.edit.FormView):
    """A view to submit webpep requests via DASF."""

    form_class = forms.WebPepForm

    permission_required = "dasf_broker.can_produce"

    template_name = "webpep/brokertopic_send_form.html"

    @cached_property
    def topic(self) -> BrokerTopic:
        return get_object_or_404(BrokerTopic, slug=self.dasf_topic_slug)

    def get_success_url(self) -> str:
        return reverse("webpep:response", args=(self.response_topic,))

    @cached_property
    def response_topic(self) -> str:
        """A random response topic"""
        topic_name = self.dasf_topic_slug

        return topic_name + "_" + get_random_letters(8)

    @property
    def dasf_topic_slug(self) -> str:
        return self.kwargs["slug"]

    @property
    def permission_object(self):
        return self.topic

    def get_initial(self) -> Dict[str, Any]:
        ret = super().get_initial()
        params = self.request.GET.dict()
        type_map = {
            "orofilter": bool,
            "sgsl": bool,
            "urban": bool,
            "origin_lon": float,
            "origin_lat": float,
            "dlon": float,
            "dlat": float,
            "startlon": float,
            "startlat": float,
            "nlon": int,
            "nlat": int,
        }
        for param, _type in type_map.items():
            if param in params:
                params[param] = _type(params[param])
        ret["constructor_arguments"] = params
        return ret

    def form_valid(self, form):
        data = form.cleaned_data["constructor_arguments"]
        data["class_name"] = "ExtParClient"
        data["function"] = {"func_name": "generate_and_upload_files"}

        payload = base64.b64encode(json.dumps(data).encode("utf-8")).decode(
            "utf-8"
        )

        request = {
            "messageId": str(uuid4()),
            "properties": {
                "response_topic": self.response_topic,
                "requestContext": 1,
                "messageType": "request",
            },
            "payload": payload,
        }

        self.topic.create_and_send_message(self.request.user, request)

        return super().form_valid(form)

    def get_context_data(self, **kwargs: Any) -> Dict[str, Any]:
        return super().get_context_data(
            topic_slug=self.dasf_topic_slug,
            preview_topic=self.response_topic + "_preview",
            container_class="container-fluid mx-4",
            **kwargs,
        )


class WebPepResponseView(PermissionRequiredMixin, generic.DetailView):
    """A view on a response topic."""

    model = ResponseTopic

    permission_required = "dasf_broker.can_consume"

    template_name = "webpep/responsetopic_detail.html"

    def get_permission_object(self):
        return super().get_permission_object().brokertopic_ptr


class BrokerTopicView(PermissionRequiredMixin, generic.DetailView):
    """A view on the available tools."""

    model = BrokerTopic

    permission_required = "dasf_broker.can_produce"

    template_name = "webpep/brokertopic_detail.html"

    def get_context_data(self, **kwargs) -> dict[str, Any]:
        context = super().get_context_data(**kwargs)
        context["topic"] = app_settings.DJAC_WEBPEP_TOOLS_TOPIC
        context["ws_url"] = app_settings.DJAC_WEBPEP_WEBSOCKET_URL_ROUTE
        context["base_breadcrumbs"] = [("CLM Tools", self.request.path)]
        return context


class BrokerTopicFunctionView(BrokerTopicView):
    """A view on a function."""

    def get_context_data(self, **kwargs) -> dict[str, Any]:
        context = super().get_context_data(**kwargs)
        context["func_name"] = self.kwargs["function_slug"]
        return context
