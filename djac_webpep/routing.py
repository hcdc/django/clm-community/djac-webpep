# SPDX-FileCopyrightText: 2022-2024 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2
"""Websocket Routing
-----------------
Websocket routing configuration for the djac-webpep app.

It exposes the `websocket_urlpatterns` list, a list of url patterns to be used
for deployment.

See Also
--------
https://channels.readthedocs.io/en/stable/topics/routing.html
"""


from typing import Any, List

from channels.routing import URLRouter
from django.urls import path, re_path

from djac_webpep import app_settings

from . import consumers

websocket_urlpatterns: List[Any] = [
    path(
        app_settings.DJAC_WEBPEP_WEBSOCKET_URL_ROUTE,
        URLRouter(
            [
                # routes of the djac-webpep app
                re_path(
                    r"^(?P<slug>webpep[-_\w]+)/?$",
                    consumers.WebPepTopicProducer.as_asgi(),
                ),
            ]
        ),
    )
]
