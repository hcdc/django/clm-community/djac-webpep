# SPDX-FileCopyrightText: 2022-2024 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

"""Django App to integrate the Helmholtz AAI into DJAC

An integration for WebPEP for the Django-Academic-Community Tool
"""

from __future__ import annotations

from . import _version

__version__ = _version.get_versions()["version"]

__author__ = "Philipp S. Sommer"
__copyright__ = "2022-2024 Helmholtz-Zentrum hereon GmbH"
__credits__ = [
    "Philipp S. Sommer",
]
__license__ = "EUPL-1.2"

__maintainer__ = "Philipp S. Sommer"
__email__ = "philipp.sommer@hereon.de"

__status__ = "Pre-Alpha"
