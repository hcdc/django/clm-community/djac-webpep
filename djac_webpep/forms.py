# SPDX-FileCopyrightText: 2022-2024 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

"""Forms
-----

Forms of the djac-webpep app.
"""

from django import forms
from django_reactive.widget.fields import ReactJSONSchemaFormField
from django_reactive.widget.widgets import ReactJSONSchemaFormWidget

schema = {
    "type": "object",
    "properties": {
        "origin_lon": {
            "title": "Origin Lon",
            "description": "The geographical longitude of the origin (0,0) of the rotated\nsystem.",
            "type": "number",
        },
        "origin_lat": {
            "title": "Origin Lat",
            "description": "The geographical latitude of the origin (0,0) of the rotated\nsystem.",
            "type": "number",
        },
        "dlon": {
            "title": "Dlon",
            "description": "The grid mesh size in degrees in the rotated coordinate system in\nx-direction.",
            "type": "number",
        },
        "dlat": {
            "title": "Dlat",
            "description": "The grid mesh size in degrees in the rotated coordinate system in\ny-direction.",
            "type": "number",
        },
        "nlon": {
            "title": "Nlon",
            "description": "The total number of grid points in X-direction. Maximum number is\n3000.",
            "type": "integer",
        },
        "nlat": {
            "title": "Nlat",
            "description": "The total number of grid points in Y-direction. Maximum number is\n3000.",
            "type": "integer",
        },
        "startlon": {
            "title": "Startlon",
            "description": "The longitude of the lower left corner of the domain in rotated\ncoordinates. The value of STARTLON is counted by starting with zero\nat origin lon.",
            "type": "number",
        },
        "startlat": {
            "title": "Startlat",
            "description": "The latitude of the lower left corner of the domain in rotated\ncoordinates. The value of STARTLAT is counted by starting with zero\nat origin lat.",
            "type": "number",
        },
        "oro": {
            "title": "Oro",
            "description": "The preferred orography data set.\n\n- GLOBE GLOBE orography\n- ASTER ASTER orography. Valid for the region 60S to 60N. Grid\n  width is restricted to <0.05 degrees.",
            "default": "GLOBE",
            "enum": ["GLOBE", "ASTER", "MERIT"],
            "type": "string",
        },
        "landuse": {
            "title": "Landuse",
            "description": "The preferred land use data set.\n\n- GLC2000 land use\n- GLOBCOVER land use\n- ECOCLIMAP land use",
            "default": "GLOBCOVER",
            "enum": ["GLOBCOVER", "GLC2000", "ECOCLIMAP"],
            "type": "string",
        },
        "soil": {
            "title": "Soil",
            "description": "Soil characteristics data set\n\n- FAO-DSMW FAO-DSMW data set\n- HWSD TERRA HWSD data TERRA mapping 1D",
            "default": "FAO-DSMW",
            "enum": ["FAO-DSMW"],
            "type": "string",
        },
        "aot": {
            "title": "Aot",
            "description": "The preferred data set for the aerosol climatology.\n\n- NASA/GISS NASA/GISS data set\n- AeroCom1, MPI_MET AeroCom1, MPI_MET data set",
            "default": "NASA/GISS",
            "enum": ["NASA/GISS", "AeroCom1, MPI_MET"],
            "type": "string",
        },
        "albedo": {
            "title": "Albedo",
            "description": "The preferred data set for use as the solar surface albedo.\n\n- MODIS dry & sat only dry and saturated soil albedo (itype_albedo=2)\n- MODIS12 vis MODIS monthly albedo, visible part (itype_albedo=3) !! only available for versions COSMO-CLM_clm3 and newer !!",
            "default": "MODIS dry & sat",
            "enum": ["MODIS dry & sat", "MODIS12 vis"],
            "type": "string",
        },
        "orofilter": {
            "title": "Orofilter",
            "description": "Enable filtering the orography here or later in INT2LM.\n\n- No:  No Filtering applied (default)\n- Yes: Filtering applied. In this case the pre-settings are as follows:\n\n  ilow_pass_oro=4,\n  numfilt_oro=1,\n  ilow_pass_xso=5,\n  lxso_first=.FALSE.,\n  numfilt_xso=1,\n  rxso_mask=750.0,\n  eps_filter=0.1,\n  rfill_valley=0.0,\n  ifill_valley=1",
            "default": False,
            "type": "boolean",
        },
        "sgsl": {
            "title": "Sgsl",
            "description": "Calculate subgrid-scale parameter S_ORO.\n\n- No: No calculating of S_ORO (default)\n- Yes: S_ORO will be calculated (this option is presently not stable, for certain domains/resolutions etc. it may crash)",
            "default": False,
            "type": "boolean",
        },
        "urban": {
            "title": "Urban",
            "description": "Include surface properties for use in TERRA_URB.\nNote: There is not yet a check for valid lon/lat boundaries!!\n\n- No: No urban parameters. Choose this for polar regions.\n- Yes: fine resolution background data for ISA, valid for mostly\n  Europe 60W-60E, 75N-12N",
            "default": False,
            "type": "boolean",
        },
    },
    "required": [
        "origin_lon",
        "origin_lat",
        "dlon",
        "dlat",
        "nlon",
        "nlat",
        "startlon",
        "startlat",
    ],
}


class WebPepForm(forms.Form):
    """A form for a webpep call."""

    constructor_arguments = ReactJSONSchemaFormField(
        widget=ReactJSONSchemaFormWidget(schema=schema)
    )
