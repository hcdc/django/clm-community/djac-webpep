# SPDX-FileCopyrightText: 2022-2024 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

"""URL config
----------

URL patterns of the djac-webpep to be included via::

    from django.urls import include, path

    urlpatters = [
        path(
            "djac-webpep",
            include("djac_webpep.urls"),
        ),
    ]
"""
from __future__ import annotations

from typing import Any

from django.urls import re_path  # noqa: F401

from djac_webpep import app_settings, views  # noqa: F401

#: App name for the djac-webpep to be used in calls to
#: :func:`django.urls.reverse`
app_name = "webpep"

#: urlpattern for the Helmholtz AAI
urlpatterns: list[Any] = [
    re_path(
        r"^(?P<slug>%s)/?$" % app_settings.DJAC_WEBPEP_WEBPEP_TOPIC,
        views.WebPepView.as_view(),
        name="webpep",
    ),
    re_path(
        r"^(?P<slug>%s[-_\w]+)/?$" % app_settings.DJAC_WEBPEP_WEBPEP_TOPIC,
        views.WebPepResponseView.as_view(),
        name="response",
    ),
    re_path(
        r"^(?P<slug>%s)/?$" % app_settings.DJAC_WEBPEP_TOOLS_TOPIC,
        views.BrokerTopicView.as_view(),
        name="clm-tools",
    ),
    re_path(
        r"^(?P<slug>%s)/(?P<function_slug>[\w][-_\w]+)/?$"
        % app_settings.DJAC_WEBPEP_TOOLS_TOPIC,
        views.BrokerTopicFunctionView.as_view(),
        name="clm-tools-function",
    ),
]
