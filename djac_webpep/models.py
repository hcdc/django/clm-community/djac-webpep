# SPDX-FileCopyrightText: 2022-2024 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

"""Models
------

Models for the djac-webpep app.
"""


from __future__ import annotations

from django.db import models  # noqa: F401

from djac_webpep import app_settings  # noqa: F401
