# SPDX-FileCopyrightText: 2022-2024 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

"""WebPEP Template tags
---------------------

This module contains the template tags for the webpep templates.
"""

from __future__ import annotations

import base64
import json
from typing import TYPE_CHECKING

from django import template
from django.urls import reverse
from django.utils.http import urlencode

if TYPE_CHECKING:
    from dasf_broker.models import BrokerMessage

register = template.Library()


@register.filter
def resubmit_url(message: BrokerMessage) -> str:
    payload = base64.b64decode(message.content.get("payload", "")).decode(
        "utf-8"
    )
    try:
        request = json.loads(payload)
    except json.JSONDecodeError:
        return ""
    else:
        request.pop("class_name", None)
        request.pop("function", None)
        uri = reverse("webpep:webpep", args=("webpep",))
        return "%s?%s" % (uri, urlencode(request, doseq=True))
