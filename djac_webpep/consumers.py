# SPDX-FileCopyrightText: 2022-2024 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

"""
Consumers
---------

Consumers of the djac-webpep app to be imported via the routing
config (see :mod:`djac_webpep.routing`).
"""


from dasf_broker.consumers import TopicProducer


class WebPepTopicProducer(TopicProducer):
    """Send a notification to the consumer that the message succeded."""

    def post_response_message(self, content):
        """Send a message to the consumers."""
        from academic_community.notifications.models import SystemNotification

        topic = self.dasf_topic
        if not topic.slug.endswith("_preview"):
            consumers = topic.consumers
            SystemNotification.create_notifications(
                consumers,
                "Your WebPep request finished",
                "webpep/request_finished_mail.html",
                {"topic": topic},
            )
