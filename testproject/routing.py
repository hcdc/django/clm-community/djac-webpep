# SPDX-FileCopyrightText: 2022-2024 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: CC0-1.0

"""
Websocket routing configuration for the djac-webpep project.

It exposes the `websocket_urlpatterns` list, a list of url patterns to be used
for deployment.

See Also
--------
https://channels.readthedocs.io/en/stable/topics/routing.html
"""

from typing import Any, List

import academic_community.routing
import dasf_broker.routing as dasf_routing
from channels.routing import URLRouter  # noqa: F401
from dasf_broker.app_settings import DASF_WEBSOCKET_URL_ROUTE
from django.urls import path  # noqa: F401

import djac_webpep.routing

websocket_urlpatterns: List[Any] = (
    academic_community.routing.websocket_urlpatterns
    + djac_webpep.routing.websocket_urlpatterns
    + [
        path(
            DASF_WEBSOCKET_URL_ROUTE,
            URLRouter(dasf_routing.websocket_urlpatterns),
        ),
    ]
)
