# SPDX-FileCopyrightText: 2022-2024 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: CC0-1.0

"""
Websocket routing configuration for the djac-webpep project.

It exposes the `workers` dictionary, a mapping from worker name
consumer to be used by channels `ChannelNameRouter`.

See Also
--------
https://channels.readthedocs.io/en/stable/topics/worker.html#receiving-and-consumers
"""
from typing import Any, Dict

import academic_community.workers

workers: Dict[str, Any] = {}
workers.update(academic_community.workers.workers)
