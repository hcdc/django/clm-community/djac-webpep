.. SPDX-FileCopyrightText: 2022-2024 Helmholtz-Zentrum hereon GmbH
..
.. SPDX-License-Identifier: CC-BY-4.0

.. _configuration:

Configuration options
=====================

Configuration settings
----------------------

The following settings have an effect on the app.

.. automodulesumm:: djac_webpep.app_settings
    :autosummary-no-titles:
