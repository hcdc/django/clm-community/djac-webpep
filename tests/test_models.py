# SPDX-FileCopyrightText: 2022-2024 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

"""Tests for the djac-webpep models
--------------------------------
"""


def test_model():
    """Place holder test to make sure pytest finds at least one.

    Delete this once you really start to write some tests.
    """
    pass
